//console.log("Hello it's me")

//MINI ACTIVITY
/*
	1. Using the es6 update, get the cube of 8.
	2. Print the reesult on the console with message. 'The interest rate on your savings account is' + result
	3. Use the template literal in printing out the message

	console print: 
	The cube of 8 is + result


*/

const getCube = 8 ** 3;
console.log(`The cube of 8 is ${getCube} `);


//MINI-ACTIVITY
/*
1. Destructure the address array
2. print the values in the console. I live at 258 Washing Avenue, California, 99011.

*/
const address = ["258", "Washington Ave NW", "California", "99011"]
const [houseNumber, street, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${street} ${state} ${zipCode}`)

//Mini-activity
const animal = {

	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: '20ft 3 in'
}
const {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement ${measurement}`)


//Mini-activity

/*
1. Loop through the numbers using forEach using arrow function
2. Print the numbers in the console
3. Use the .reduce operator on numbers array.
4. Assign the result on the variable
5. print the variable on the console.

*/

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((num) => {
	console.log(`${num}`)
})

let sum = numbers.reduce((acc, num) => {
	return acc + num;
});

console.log(sum);

/*answer from Sir
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((num) => console.log(number))
let reduceNumber = numbers.reduce((x ,y) => x + y);
console.log(reduceNumber);

*/


// MINI-ACTIVITY
/*
1. Create a "dog" class
2. Inside of the class "dog", have a name, age and breed
3. Instantiate a new dog class and print in the console
4. Send the screenshot in the hangouts.
*/


class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

let myDog = new Dog("Bernard", 3, "Poodle");
console.log(myDog);